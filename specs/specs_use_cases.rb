# frozen_string_literal: true
require "minitest/autorun"
require_relative "../lib/robot_simulator.rb"

describe "run multiple commands and check the report" do
  before do
    @rs = RobotSimulator.new
  end

  it "moves and checks new position (use case 1)" do
    @rs.enter_command("PLACE 0,0,NORTH")
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 0,1,NORTH"
  end

  it "moves and checks new position (use case 2)" do
    @rs.enter_command("PLACE 0,0,NORTH")
    @rs.enter_command("LEFT")
    @rs.enter_command("REPORT").must_equal "Output: 0,0,WEST"
  end

  it "moves and checks new position (use case 3)" do
    @rs.enter_command("PLACE 1,2,EAST")
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE")
    @rs.enter_command("LEFT")
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 3,3,NORTH"
  end

  it "moves and checks new position (use case 4)" do
    @rs.enter_command("PLACE 2,3,EAST")
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 4,3,EAST"
    @rs.enter_command("MOVE").must_equal "Can't move! The end of the table."
    @rs.enter_command("RIGHT")
    @rs.enter_command("REPORT").must_equal "Output: 4,3,SOUTH"
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 4,1,SOUTH"
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE").must_equal "Can't move! The end of the table."
    @rs.enter_command("RIGHT")
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE 1,1,SOUTH")
       .must_equal "Invalid command: MOVE 1,1,SOUTH. Press Ctrl-D to exit."
    @rs.enter_command("MOVE")
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 1,0,WEST"
    @rs.enter_command("MOVE")
    @rs.enter_command("RIGHT")
    @rs.enter_command("REPORT").must_equal "Output: 0,0,NORTH"
  end

  it "places the robot several times and checks new position (use case 5)" do
    @rs.enter_command("PLACE 1,2,EAST")
    @rs.enter_command("REPORT").must_equal "Output: 1,2,EAST"
    @rs.enter_command("MOVE")
    @rs.enter_command("REPORT").must_equal "Output: 2,2,EAST"
    @rs.enter_command("PLACE 3,4,SOUTH")
    @rs.enter_command("REPORT").must_equal "Output: 3,4,SOUTH"
    @rs.enter_command("PLACE 0,0,NORTH")
    @rs.enter_command("REPORT").must_equal "Output: 0,0,NORTH"
  end
end
