# frozen_string_literal: true
require "minitest/autorun"
require_relative "../lib/robot_facing.rb"

describe "robot facing" do
  before do
    @face = RobotFacing.new
  end

  describe "robot has not been faced yet" do
    it "checks the robot orientation before it's faced" do
      @face.facing.must_be_nil
    end

    it "checks the faced? value before the robot's faced" do
      @face.faced?.must_equal false
    end
  end

  describe "robot has been faced" do
    before do
      @face.face("NORTH")
    end

    it "checks the robot orientation" do
      @face.facing.must_equal "NORTH"
    end

    it "checks if the robot is placed?" do
      @face.faced?.must_equal true
    end
  end

  describe "robot turns left" do
    before do
      @face.face("EAST")
    end

    it "turns the robot once" do
      @face.left
      @face.facing.must_equal "NORTH"
    end

    it "turns the robot twice" do
      2.times { @face.left }
      @face.facing.must_equal "WEST"
    end

    it "turns the robot 4 times" do
      4.times { @face.left }
      @face.facing.must_equal "EAST"
    end
  end

  describe "robot turns right" do
    before do
      @face.face("SOUTH")
    end

    it "turns the robot once" do
      @face.right
      @face.facing.must_equal "WEST"
    end

    it "turns the robot twice" do
      2.times { @face.right }
      @face.facing.must_equal "NORTH"
    end

    it "turns the robot 4 times" do
      4.times { @face.right }
      @face.facing.must_equal "SOUTH"
    end
  end
end
