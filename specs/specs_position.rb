# frozen_string_literal: true
require "minitest/autorun"
require_relative "../lib/robot_position.rb"

describe "robot position" do
  before do
    @robot = RobotPosition.new
  end

  describe "robot has not been placed yet" do
    it "checks the robot position before it's placed" do
      @robot.position.must_be_nil
    end

    it "checks the placed? value before the robot's placed" do
      @robot.placed?.must_equal false
    end
  end

  describe "robot has been placed" do
    before do
      @robot.place(1, 2)
    end

    it "checks the robot position" do
      @robot.position.must_equal [1, 2]
    end

    it "checks if the robot is placed?" do
      @robot.placed?.must_equal true
    end
  end
end
