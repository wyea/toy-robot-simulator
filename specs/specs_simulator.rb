# frozen_string_literal: true
require "minitest/autorun"
require_relative "../lib/robot_simulator.rb"

describe "run commands" do
  before do
    @rs = RobotSimulator.new
  end

  # make sure that valid commads are accepted
  describe "valid commands" do
    it "accepts the PLACE command with arguments" do
      @rs.enter_command("PLACE 1,1,NORTH").must_be_nil
    end

    describe "accepts MOVE, LEFT, RIGHT, REPORT if the robot is placed?" do
      before do
        @rs.enter_command("PLACE 1,1,NORTH")
      end

      describe "check the MOVE command" do
        it "moves once and checks new position" do
          @rs.enter_command("MOVE")
          @rs.enter_command("REPORT").must_equal "Output: 1,2,NORTH"
        end

        it "moves 3 times and checks new position" do
          3.times { @rs.enter_command("MOVE") }
          @rs.enter_command("REPORT").must_equal "Output: 1,4,NORTH"
        end
      end

      it "accepts the LEFT command with no arguments" do
        @rs.enter_command("LEFT").must_be_nil
      end

      it "accepts the RIGHT command with no arguments" do
        @rs.enter_command("RIGHT").must_be_nil
      end

      describe "check the REPORT command" do
        it "calls the REPORT command" do
          @rs.enter_command("REPORT").must_equal "Output: 1,1,NORTH"
        end

        it "turns left and calls the REPORT command" do
          @rs.enter_command("LEFT")
          @rs.enter_command("REPORT").must_equal "Output: 1,1,WEST"
        end
      end
    end
  end

  # make sure that invalid command are not accepted
  describe "invalid commands" do
    describe "check moving outside the table" do
      it "places the robot and moves outside the table" do
        @rs.enter_command("PLACE 0,0,WEST")
        @rs.enter_command("MOVE")
        @rs.enter_command("MOVE").must_equal "Can't move! The end of the table."
        @rs.enter_command("REPORT").must_equal "Output: 0,0,WEST"
      end
    end

    it "doesn't accept an invalid command" do
      @rs.enter_command("BARK")
         .must_equal "Invalid command: BARK. Press Ctrl-D to exit."
    end

    it "doesn't accept an invalid argument(face)" do
      @rs.enter_command("PLACE 1,1,DOWN")
         .must_equal "Invalid command: PLACE 1,1,DOWN. Press Ctrl-D to exit."
    end

    it "doesn't accept an invalid argument(coordinates)" do
      @rs.enter_command("PLACE 1,7,SOUTH")
         .must_equal "Invalid command: PLACE 1,7,SOUTH. Press Ctrl-D to exit."
    end

    it "doesn't accept an invalid number of argument" do
      @rs.enter_command("PLACE 1,1,2,SOUTH")
         .must_equal "Invalid command: PLACE 1,1,2,SOUTH. Press Ctrl-D to exit."
    end

    it "doesn't accept a non-PLACE command with an argument" do
      @rs.enter_command("MOVE 1,1,SOUTH")
         .must_equal "Invalid command: MOVE 1,1,SOUTH. Press Ctrl-D to exit."
    end

    describe "robot has not been placed yet" do
      it "doesn't move" do
        @rs.enter_command("MOVE")
           .must_equal "Please place and face the robot first!"
      end

      it "doesn't turn left" do
        @rs.enter_command("LEFT")
           .must_equal "Please place and face the robot first!"
      end

      it "doesn't turn right" do
        @rs.enter_command("RIGHT")
           .must_equal "Please place and face the robot first!"
      end

      it "doesn't report" do
        @rs.enter_command("REPORT")
           .must_equal "Please place and face the robot first!"
      end
    end
  end
end
