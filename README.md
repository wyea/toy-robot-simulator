## Toy Robot Simulator
A robot that lives on a 5 x 5 table.

##### Installation
Before installing the Toy Robot Simulator, please make sure that you have installed the following software:

1. Ruby. The simulator uses Ruby 2.3.1. Please see [the link](https://www.ruby-lang.org/en/downloads/) for installation instructions. For Linux and OS X systems, please consider using [rbenv](https://github.com/rbenv/rbenv), [RVM](https://rvm.io/rvm/install), [chruby](https://github.com/postmodern/chruby) or another version management system.
2. Bundler. To install Bundler please run `gem install bundler`. You will need it to run `bundle install`. **Otherwise**, please install gems listed in the Gemfile manually with `gem install gem_name`. Replace `gem_name` with a gem name.
3. Git. Please see [the link](https://git-scm.com/downloads) for installation instructions. **Otherwise**, please [download](https://bitbucket.org/wyea/toy-robot-simulator/downloads) and unpack the repository manually.

To install the simulator locally, please do the following:
```sh
git clone https://wyea@bitbucket.org/wyea/toy-robot-simulator.git
cd toy-robot-simulator
bundle install
```
Congratulations! The Toy Robot Simulator is installed and ready to use!

##### How to use it
Once you're in the `toy-robot-simulator` directory, simply run `ruby lib/robot_interactor.rb`. To exit the program, please press Ctrl-D anytime.

###### Commands you can use:
1. `PLACE X,Y,F`. Where X and Y are coordinates (0..4) and F is facing (NORTH, EAST, SOUTH, WEST) of the robot on the table. **Note**, you have to place the robot first. You can't use any other commands until it's placed!
2. `MOVE`. Make one step towards the faced direction (if possible).
3. `LEFT`. Make a 90-degree-anticlockwise turn. NORTH => WEST => SOUTH => EAST => NORTH.
4. `RIGHT`. Make a 90-degree-clockwise turn. NORTH => EAST => SOUTH => WEST => NORTH.
5. `REPORT`. Show the current position. For example, `Output: 3,3,NORTH`.

So, you experience can look like this:
```
ruby lib/robot_interactor.rb
Press Ctrl-D to exit.
Please PLACE X,Y,F the Robot first.
X and Y are (0..4).
F is NORTH, EAST, SOUTH or WEST.

PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
Output: 3,3,NORTH
```

##### Testing
To test the program, simply run `rake test`.