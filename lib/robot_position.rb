# frozen_string_literal: true
# Toy Robot position on the table
class RobotPosition
  attr_accessor :position

  # Places the robot
  def place(x, y)
    self.position = [x, y]
  end

  # Checks if the robot is placed
  def placed?
    !position.nil?
  end
end
