# frozen_string_literal: true
# Toy Robot facing on the table
class RobotFacing
  attr_accessor :facing

  # Faces the robot to NORTH, EAST, SOUTH or WEST
  def face(f)
    self.facing = f
  end

  # Checks if the robot is faced
  def faced?
    !facing.nil?
  end

  # Turns the robot to the left (anticlockwise) without changing its position
  def left
    left_turn   = { "NORTH" => "WEST", "WEST" => "SOUTH",
                    "SOUTH" => "EAST", "EAST" => "NORTH" }
    self.facing = left_turn[facing]
  end

  # Turns the robot to the right (clockwise) without changing its position
  def right
    right_turn  = { "NORTH" => "EAST", "EAST" => "SOUTH",
                    "SOUTH" => "WEST", "WEST" => "NORTH" }
    self.facing = right_turn[facing]
  end
end
