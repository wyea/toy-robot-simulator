# frozen_string_literal: true
require_relative "robot_position"
require_relative "robot_facing"

# Toy Robot Simulator Application
class RobotSimulator
  def initialize
    @robot = RobotPosition.new
    @face  = RobotFacing.new
  end

  def enter_command(command)
    unless validate_command(command)
      return "Invalid command: #{command}. Press Ctrl-D to exit."
    end
    command_words = command.split(" ")
    command_verb  = command_words.first
    command_args  = command_words.last

    execute_command(command_verb, command_args)
  end

  private

  # Validate the command entered with regex
  def validate_command(command)
    command_regex = /
      \A
      (PLACE|MOVE|LEFT|RIGHT|REPORT) # matches one of the commands
      ((?<=PLACE) # if the command is PLACE
      \s([0-4]\,){2} # than make sure that coordinates
      (NORTH|SOUTH|EAST|WEST)| # and facing in the argument are correct
      (?<!PLACE)$) # otherwise, make sure that there is no argument
      \z
    /x
    command_regex =~ command # check if the command matches the regex
  end

  # Executes the right command
  def execute_command(command_verb, command_args)
    case command_verb
    when "PLACE"  then place(command_args)
    when "MOVE"   then move
    when "LEFT"   then left
    when "RIGHT"  then right
    when "REPORT" then report
    end
  end

  # Places the robot to the table
  def place(command_args)
    command_args_array = command_args.split(",")
    x, y, f = command_args_array.map { |e| e.to_i.to_s == e ? e.to_i : e }
    return "Invalid coordinates!" unless validate_coordinates(x, y)
    return "Invalid facing!"      unless validate_facing(f)
    return "The robot wasn't placed!" unless @robot.place(x, y) && @face.face(f)
    nil
  end

  # Makes sure that the robot is on the table
  def validate_coordinates(x, y)
    x >= 0 && y >= 0 && x <= 4 && y <= 4
  end

  # Makes sure that the facing is correct (NORTH, EAST, SOUTH or WEST)
  def validate_facing(f)
    %w(NORTH EAST SOUTH WEST).include?(f)
  end

  # Makes a step to the specified direction
  def move
    unless @robot.placed? && @face.faced?
      return "Please place and face the robot first!"
    end
    current_position = @robot.position
    new_x, new_y = current_position.zip(find_the_way).map { |a, b| a + b }
    unless validate_coordinates(new_x, new_y) && @robot.place(new_x, new_y)
      return "Can't move! The end of the table."
    end
    nil
  end

  # Finds the way to move based on the robot's facing
  def find_the_way
    case @face.facing
    when "NORTH" then [0,  1]
    when "EAST"  then [1,  0]
    when "SOUTH" then [0, -1]
    when "WEST"  then [-1, 0]
    end
  end

  # Turns left (anticlockwise)
  def left
    unless @robot.placed? && @face.faced?
      return "Please place and face the robot first!"
    end
    @face.left
    nil
  end

  # Turns right (clockwise)
  def right
    unless @robot.placed? && @face.faced?
      return "Please place and face the robot first!"
    end
    @face.right
    nil
  end

  # Prints the current position on the robot
  def report
    unless @robot.placed? && @face.faced?
      return "Please place and face the robot first!"
    end
    position = @robot.position
    facing   = @face.facing
    "Output: #{position.join(',')},#{facing}"
  end
end
