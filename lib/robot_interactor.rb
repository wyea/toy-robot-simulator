# frozen_string_literal: true
require_relative "robot_simulator"

rs = RobotSimulator.new
puts "Press Ctrl-D to exit."
puts "Please PLACE X,Y,F the Robot first."
puts "X and Y are (0..4)."
puts "F is NORTH, EAST, SOUTH or WEST."
puts

command = ""

# If user presses Crtl-D (= nil) the loop stops executing
while command
  command = gets
  if command
    message = rs.enter_command(command.upcase.chomp)
    puts message if message
  end
end
